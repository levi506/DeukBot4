﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Discord;

namespace DeukBot4
{

    public class Logger
    {
        public static Logger Main = new Logger();

        private readonly Dictionary<LogSeverity, ConsoleColor> Colors = new Dictionary<LogSeverity, ConsoleColor>
        {
            {LogSeverity.Info, ConsoleColor.Black},
            {LogSeverity.Verbose, ConsoleColor.Black},
            {LogSeverity.Debug, ConsoleColor.Black},
            {LogSeverity.Warning, ConsoleColor.Yellow},
            {LogSeverity.Error, ConsoleColor.Red},
            {LogSeverity.Critical, ConsoleColor.Red}
        };

        public async Task Log(object o, LogSeverity severity)
        {
            Console.ForegroundColor = Colors[severity];
            Console.WriteLine($"[{severity}] {DateTime.UtcNow:u}: {o.ToString()}");
            Console.ResetColor();
        }

        public async Task LogDiscord(LogMessage message)
        {
            Console.ForegroundColor = Colors[message.Severity];
            Console.WriteLine($"[{message.Severity}] {DateTime.UtcNow:u}: {message.Message}");
            Console.ResetColor();
        }

        public async Task Log(object o)
        {
            await Log(o, LogSeverity.Info);
        }

        public async Task LogWarning(object o)
        {
            await Log(o, LogSeverity.Warning);
        }

        public async Task LogError(object o)
        {
            await Log(o, LogSeverity.Error);
        }
    }
}