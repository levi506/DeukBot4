﻿using System;
using System.Threading.Tasks;
using DeukBot4.MessageHandlers.JokeHandling;
using Discord;
using Discord.WebSocket;

namespace DeukBot4.MessageHandlers
{
    public static class MainHandler
    {
        public static async Task HandleMessage(SocketMessage message)
        {
            if (message.Author.IsBot)
                return;
            if (message.Author.Id == Program.Client.CurrentUser.Id)
            {
                return;
            }
            try
            {
                var receivedMessage = new ReceivedMessage(message);
#pragma warning disable 4014
                CommandHandler.CommandHandler.HandleMessage(receivedMessage);
                HandlePrivateMessage(receivedMessage);
                ReminderHandler.HandleReminder(receivedMessage);
                ImageBackupHandler.Backup(receivedMessage);
                JokeHandler.RunJokes(receivedMessage);
                PointsMessageHandler.HandleMessage(receivedMessage);
#pragma warning restore 4014
            }
            catch (Exception e)
            {
                await Logger.Main.LogError(e.ToString());
            }
        }

        private static ITextChannel _dmChannel;
        private static async Task HandlePrivateMessage(ReceivedMessage receivedMessage)
        {
            var message = receivedMessage.Message;
            if (message.Channel is IPrivateChannel)
            {
                await Logger.Main.Log(($"Private Message: {message.Author.Username}- {message.Content}"));
                if (_dmChannel == null)
                {
                    _dmChannel = (ITextChannel) Program.Client.GetChannel(Program.Settings.DmChannel);
                }
                var eb = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        Name = message.Author.Username,
                        IconUrl = message.Author.GetAvatarUrl(),
                    },
                    Description = message.Content,
                    Color       = Color.Gold,
                    Timestamp = message.CreatedAt,
                    Footer = new EmbedFooterBuilder
                    {
                        Text = $"User ID: {message.Author.Id}"
                    }
                };
                await _dmChannel.SendMessageAsync("", embed: eb.Build());
            }
        }

    }
}