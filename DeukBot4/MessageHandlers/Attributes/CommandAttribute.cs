﻿using System;
using DeukBot4.MessageHandlers.Permissions;
using JetBrains.Annotations;

namespace DeukBot4.MessageHandlers
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    [MeansImplicitUse]
    public class CommandAttribute : Attribute
    {
        public string Command { get; }
        public PermissionLevel Permission { get; }

        public CommandAttribute(string command, PermissionLevel permission)
        {
            Command = command;
            Permission = permission;
        }
    }
}