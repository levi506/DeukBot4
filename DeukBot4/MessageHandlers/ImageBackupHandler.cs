﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Discord;

namespace DeukBot4.MessageHandlers
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ServerChannelIDs
    {
        public ulong ServerId  { get; set; }
        public ulong ChannelId { get; set; }
    }

    public static class ImageBackupHandler
    {
        // ReSharper disable once ClassNeverInstantiated.Global
        public class WebDavSettings
        {
            public string URL { get; set; }
            public string Username { get; set; }
            public string Password { internal get; set; }
        }

        private static List<ServerChannelIDs> _backupChannels = new List<ServerChannelIDs>();
        public static WebDavSettings Settings { private get; set; }

        public static async Task Backup(ReceivedMessage receivedMessage)
        {
            var message = receivedMessage.Message;
            if (!(message.Channel is IGuildChannel serverChannel))
                return;

            var images = message.Attachments.Where(x => x.Height.HasValue).ToArray();
            if (!images.Any())
            {
                return;
            }

            var found = false;
            foreach (var backupChannel in _backupChannels)
            {
                if (serverChannel.GuildId == backupChannel.ServerId && serverChannel.Id == backupChannel.ChannelId)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
                return;

            foreach (var attachment in images)
            {
                using (var client = new WebClient())
                {
                    var attachmentSplit = attachment.Filename.Split('.');
                    var filename = $"{attachmentSplit[0]}-{DateTime.UtcNow:s}.{attachmentSplit[1]}";

                    var content = client.DownloadData(attachment.Url);
                    var uri = new Uri($"{Settings.URL}/{filename}");
                    client.Credentials = new NetworkCredential(Settings.Username, Settings.Password);
                    try
                    {
                        client.UploadData(uri, "PUT", content);
                        var eb = new EmbedBuilder()
                        {
                            Title = "Image Backed Up",
                            Description = $"Image with the name ``{attachment.Filename}`` was backed up as " +
                                          $"``{filename}``",
                            ThumbnailUrl = attachment.Url,
                            Timestamp = DateTime.UtcNow
                        };
                        await message.Channel.SendMessageAsync("", embed: eb.Build());
                    }
                    catch (Exception e)
                    {
                        await Logger.Main.LogError(e);
                    }
                }
            }
        }

        public static void SetBackupChannels(List<ServerChannelIDs> ls)
        {
            _backupChannels = ls;
        }
    }
}