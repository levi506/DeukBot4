using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DeukBot4.Database;
using DeukBot4.MessageHandlers.CommandHandler;
using Discord;

namespace DeukBot4.MessageHandlers.JokeHandling
{
    public class OwoHandler : IJokeController
    {
        public string Id => "owo-warn";
        public string Name => "Owo Warnings";

        private static readonly Regex Matcher = new Regex(@"(?:^|\s)(.)[wv]\1(?:\s|$)", RegexOptions.IgnoreCase);

        public async Task Run(ReceivedMessage receivedMessage)
        {
            if (!(receivedMessage.Message.Channel is IGuildChannel serverChannel))
                return;

            if (!Matcher.Match(receivedMessage.Message.Content).Success)
                return;
            if (receivedMessage.IsHandled)
                return;
            receivedMessage.IsHandled = true;
            var message = receivedMessage.Message;

            var changePoints = await PointHandler.ChangePoints(serverChannel.GuildId, message.Author.Id, -100);
            var embed = EmbedFactory.GetStandardEmbedBuilder();
            embed.Title = "No Owoing";
            embed.Description =
                $"{message.Author.Mention}, you were fined by -100 points. Your new score is {changePoints}.";
            message.Channel.SendMessageAsync("", embed: embed.Build());
        }
    }
}