using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeukBot4.Database.ServerSettings;
using Discord;

namespace DeukBot4.MessageHandlers.JokeHandling
{
    public static class JokeHandler
    {
        public static readonly Dictionary<string, IJokeController> Jokes = new Dictionary<string, IJokeController>();

        public static void Initialize()
        {
            var jokes = typeof(JokeHandler).Assembly.GetTypes()
                .Where(x => typeof(IJokeController).IsAssignableFrom(x) && !x.IsInterface);
            foreach (var joke in jokes)
            {
                var c = (IJokeController)Activator.CreateInstance(joke);
                Jokes.Add(c.Id.ToLowerInvariant(), c);
            }
        }

        public static async Task RunJokes(ReceivedMessage message)
        {
            if (message.Message.Channel is IGuildChannel guildChannel)
            {
                var guildSettings = ServerSettingHandler.GetSettings(guildChannel.GuildId);
                var jokes = guildSettings.EnabledJokes;
                foreach (var enabledJoke in jokes)
                {
                    if (message.IsHandled)
                        return;
                    var jokeController = Jokes[enabledJoke];
                    jokeController.Run(message);
                }
            }
        }
    }
}