using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;

namespace DeukBot4.MessageHandlers.JokeHandling
{
    internal class DadJoke : IJokeController
    {
        public string Id => "dad";
        public string Name => "Hi I'm dad";

        private readonly Regex _regex = new Regex(@"(\s|^)i(`|’|'| a)?m (?<word>.{3,15})$",
            RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);

        public async Task Run(ReceivedMessage message)
        {
            if (!(message.Message.Author is IGuildUser guildUser))
                return;
            var    content = message.Message.Content;
            var match = _regex.Match(content);
            if (match.Success)
            {
                if (message.IsHandled)
                    return;
                message.IsHandled = true;
                var newName = match.Groups["word"].Value;
                await message.Message.Channel.SendMessageAsync($"Hi {newName}, i'm Deukbot");
                await guildUser.ModifyAsync(user => user.Nickname = newName);
            }
        }
    }
}