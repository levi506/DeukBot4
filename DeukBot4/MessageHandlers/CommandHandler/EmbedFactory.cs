using System;
using Discord;

namespace DeukBot4.MessageHandlers.CommandHandler
{
    public static class EmbedFactory
    {
        private static EmbedAuthorBuilder Author;

        public static void Initialize(ISelfUser user)
        {
            Author = new EmbedAuthorBuilder()
            {
                Name = user.Username,
                IconUrl = user.GetAvatarUrl(ImageFormat.Png, 128)
            };
        }

        public static EmbedBuilder GetStandardEmbedBuilder()
        {
            var eb = new EmbedBuilder
            {
                Author = Author,
                Color = Color.Gold,
                Timestamp = DateTimeOffset.Now
            };
            return eb;
        }
    }
}