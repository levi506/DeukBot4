﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;

namespace DeukBot4.MessageHandlers.CommandHandler.RequestStructure
{
    public class RequestParameter
    {
        public ParameterMatcher.ParameterType Type { get; }
        private readonly string _value;

        public RequestParameter(string value, ParameterMatcher.ParameterType type)
        {
            Type = type;
            _value = value;
        }

        public int? AsInt()
        {
            if (int.TryParse(_value, out var i))
            {
                return i;
            }
            return null;
        }

        public string AsString()
        {
            return _value;
        }

        public ulong? AsUlong()
        {
            if (ulong.TryParse(_value, out var i))
            {
                return i;
            }
            return null;
        }

        public async Task<IGuildUser> AsDiscordGuildUser(IGuild guild)
        {
            if (ulong.TryParse(_value, out var i))
            {
                return await guild.GetUserAsync(i);
            }
            else
            {
                var t1 = DateTime.UtcNow;
                var users = await guild.GetUsersAsync();
                var user = users.FirstOrDefault(x =>
                    x.Username.IndexOf(_value, StringComparison.InvariantCultureIgnoreCase) != -1);
                Logger.Main.LogWarning($"Finding user on server took {(DateTime.UtcNow - t1).TotalMilliseconds} ms");
                return user;
            }
        }

        public async Task<IUser> AsDiscordUser()
        {
            if (ulong.TryParse(_value, out var i))
            {
                return Program.Client.GetUser(i);
            }
            return null;
        }

    }
}