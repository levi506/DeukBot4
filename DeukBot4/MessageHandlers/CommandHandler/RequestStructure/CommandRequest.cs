﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DeukBot4.MessageHandlers.Permissions;
using Discord;
using Discord.Rest;
using Discord.WebSocket;

namespace DeukBot4.MessageHandlers.CommandHandler.RequestStructure
{
    public class CommandRequest
    {
        private static readonly string CommandNamePattern =
            "(?:<@!?\\d*> !*|^" + CommandHandler.CommandTrigger + "+)([^ ]+) *(.*)";
        private static readonly Regex CommandNameMatcher = new Regex(CommandNamePattern);

        public Command       Command         { get; }
        public SocketMessage OriginalMessage { get; }
        public RequestParameter[] Parameters { get; }
        public PermissionLevel RequestPermissions { get; }

        private CommandRequest(SocketMessage message, Command command, PermissionLevel requestPermissions, RequestParameter[] parameters)
        {
            OriginalMessage = message;
            Command = command;
            RequestPermissions = requestPermissions;
            Parameters = parameters;
        }

        public enum RequestCode
        {
            OK, UnknownCommand, Invalid, Forbidden, InvalidParameters
        }

        public Task<RestUserMessage> SendMessageAsync(string text, bool isTTS = false, Embed embed = null)
        {
            return OriginalMessage.Channel.SendMessageAsync(text, isTTS, embed);
        }

        public Task<RestUserMessage> SendSimpleEmbed(string title, string description, IEnumerable<EmbedFieldBuilder> fields = null)
        {
            var eb = EmbedFactory.GetStandardEmbedBuilder();
            eb.Title = title;
            eb.Description = description;
            eb.Fields = fields == null ? new List<EmbedFieldBuilder>() : fields.ToList();
            return OriginalMessage.Channel.SendMessageAsync("", embed: eb.Build());
        }

        public class CommandRequestResponse
        {
            public CommandRequest Request { get; }
            public RequestCode Response { get; }
            public string CommandName { get; }

            public CommandRequestResponse(CommandRequest request, RequestCode response, string commandName)
            {
                Request = request;
                Response = response;
                CommandName = commandName;
            }
        }


        public static async Task<CommandRequestResponse> Create(SocketMessage message)
        {
            var originalMessage = message;
            var content = message.Content;
            var res     = CommandNameMatcher.Match(content);
            if (res.Groups.Count <= 2)
                return new CommandRequestResponse(null, RequestCode.Invalid, null);

            var commandName = res.Groups[1].Value;
            var command     = CommandHandler.GetCommand(commandName);
            if (command == null)
            {
                return new CommandRequestResponse(null, RequestCode.UnknownCommand, commandName);
            }

            PermissionLevel permission;
            try
            {
                permission = await PermissionValidator.GetUserPermissionLevel(message);
            }
            catch (Exception e)
            {
                await Logger.Main.LogError(e.Message);
                return new CommandRequestResponse(null, RequestCode.Forbidden, null);
            }
            if (permission < command.Permission)
            {
                return new CommandRequestResponse(null, RequestCode.Forbidden, permission.ToString());
            }
            var parameterString = res.Groups[2].Value;
            var parameters = ParameterMatcher.GetParameterValues(command, parameterString);
            if (parameters == null)
            {
                return new CommandRequestResponse(null, RequestCode.InvalidParameters, parameterString);
            }

            return new CommandRequestResponse(new CommandRequest(originalMessage, command, permission, parameters),
                RequestCode.OK, null);
        }

    }
}