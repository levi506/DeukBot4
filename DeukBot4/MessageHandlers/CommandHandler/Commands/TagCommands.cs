using System.Threading.Tasks;
using DeukBot4.MessageHandlers.CommandHandler.RequestStructure;
using DeukBot4.MessageHandlers.Permissions;

namespace DeukBot4.MessageHandlers.CommandHandler
{
    public class TagCommands : CommandContainerBase
    {
        public override string Name => "Tags";

        [Command("createtag", PermissionLevel.Helper)]
        [CommandParameters(ParameterMatcher.ParameterType.Word, ParameterMatcher.ParameterType.Remainder)]
        [CommandHelp("Creates a tag", "Creates a tag\nRequires at least two words")]
        [BlockUsageInPm, RequireParameterMatch]
        public async Task CreateTag(CommandRequest request)
        {
            TagStorage.AddTag(request);
        }

        [Command("removetag", PermissionLevel.Helper)]
        [CommandParameters(ParameterMatcher.ParameterType.Word)]
        [CommandHelp("Removes a tag", "Removes a a tag\nRequires the key to be given")]
        [BlockUsageInPm, RequireParameterMatch]
        public async Task RemoveTag(CommandRequest request)
        {
            TagStorage.RemoveTag(request);
        }

        [Command("tag", PermissionLevel.Everyone)]
        [CommandParameters(ParameterMatcher.ParameterType.Remainder)]
        [CommandHelp("Gets a tag", "Gets a tag earlier set by a moderator.")]
        [BlockUsageInPm]
        public async Task GetTag(CommandRequest request)
        {
            TagStorage.GetTag(request);
        }


        [Command("tags", PermissionLevel.Everyone)]
        [CommandHelp("List tags in the server", "List tags in the server set by a moderator.")]
        [BlockUsageInPm]
        public async Task GetTags(CommandRequest request)
        {
            TagStorage.GetTags(request);
        }

    }
}