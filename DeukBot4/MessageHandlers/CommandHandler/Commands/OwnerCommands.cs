﻿using System.Threading.Tasks;
using DeukBot4.MessageHandlers.CommandHandler.RequestStructure;
using DeukBot4.MessageHandlers.Permissions;
using Discord;

namespace DeukBot4.MessageHandlers.CommandHandler
{
    public class OwnerCommands : CommandContainerBase
    {
        public override string Name => "Owner";

        [Command("send", PermissionLevel.BotCreator)]
        [CommandParameters(ParameterMatcher.ParameterType.Number, ParameterMatcher.ParameterType.Remainder)]
        public async Task SendMessage(CommandRequest request)
        {
            if (request.Parameters.Length < 2)
                return;
            var channelId = request.Parameters[0].AsUlong();
            if (!channelId.HasValue)
            {
                await request.SendSimpleEmbed("Send", $"Invalid channel ID: {request.Parameters[0].AsString()}");
                return;
            }
            if (!(Program.Client.GetChannel(channelId.Value) is IMessageChannel channelFind))
            {
                await request.SendSimpleEmbed("Send", $"Can't find channel: {channelId.Value}");
                return;
            }
            await channelFind.SendMessageAsync(request.Parameters[1].AsString());
        }

        [Command("dm", PermissionLevel.BotCreator)]
        [CommandParameters(ParameterMatcher.ParameterType.Number, ParameterMatcher.ParameterType.Remainder)]
        public async Task DmMessage(CommandRequest request)
        {
            if (request.Parameters.Length < 2)
                return;
            var channelId = request.Parameters[0].AsUlong();
            if (!channelId.HasValue)
            {
                await request.SendSimpleEmbed("DM", $"Invalid channel ID: {request.Parameters[0].AsString()}");
                return;
            }

            var user = Program.Client.GetUser(channelId.Value);
            if (user == null)
            {
                await request.SendSimpleEmbed("DM", $"Can't find user: {channelId.Value}");
                return;
            }

            var channel = await user.GetOrCreateDMChannelAsync();
            if (channel == null)
            {
                await request.SendSimpleEmbed("DM", $"Can't create channel");
                return;
            }
            await channel.SendMessageAsync(request.Parameters[1].AsString());
        }

    }
}