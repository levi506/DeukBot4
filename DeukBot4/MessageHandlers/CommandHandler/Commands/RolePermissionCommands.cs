﻿using System;
using System.Text;
using System.Threading.Tasks;
using DeukBot4.Database;
using DeukBot4.Database.ServerSettings;
using DeukBot4.MessageHandlers.CommandHandler.RequestStructure;
using DeukBot4.MessageHandlers.Permissions;
using Discord;

namespace DeukBot4.MessageHandlers.CommandHandler
{
    public class RolePermissionCommands : CommandContainerBase
    {
        public override string Name => "Role Related";

        [Command("roles", PermissionLevel.Admin)]
        [CommandHelp("Lists all roles on the server along with their IDs", "Lists all roles on the server along with their IDs")]
        [BlockUsageInPm]
        public async Task ListServerRoles(CommandRequest request)
        {
            var channel = request.OriginalMessage.Channel;
            if (!(channel is IGuildChannel serverChannel))
                return;

            var roles = serverChannel.Guild.Roles;
            var sb = new StringBuilder();
            foreach (var role in roles)
            {
                sb.Append($"``{role.Name}`` --> {role.Id}\n");
            }
            await channel.SendMessageAsync(sb.ToString());
        }

        [Command("adminrole", PermissionLevel.ServerOwner)]
        [CommandParameters(ParameterMatcher.ParameterType.Number)]
        [BlockUsageInPm]
        public async Task SetAdminRole(CommandRequest request)
        {
            await SetRolePermission(request, PermissionLevel.Admin);
        }

        [Command("moderatorrole", PermissionLevel.ServerOwner)]
        [CommandParameters(ParameterMatcher.ParameterType.Number)]
        [BlockUsageInPm]
        public async Task SetModRole(CommandRequest request)
        {
            await SetRolePermission(request, PermissionLevel.Moderator);
        }

        [Command("helperrole", PermissionLevel.ServerOwner)]
        [CommandParameters(ParameterMatcher.ParameterType.Number)]
        [BlockUsageInPm]
        public async Task SetHelperRole(CommandRequest request)
        {
            await SetRolePermission(request, PermissionLevel.Helper);
        }

        [Command("blockedrole", PermissionLevel.ServerOwner)]
        [CommandParameters(ParameterMatcher.ParameterType.Number)]
        [BlockUsageInPm]
        public async Task SetBannedRole(CommandRequest request)
        {
            await SetRolePermission(request, PermissionLevel.Banned);
        }


        private static async Task SetRolePermission(CommandRequest request, PermissionLevel permissionLevel)
        {
            var channel = request.OriginalMessage.Channel;
            if (!(channel is IGuildChannel serverChannel))
                return;

            if (request.Parameters.Length == 0)
            {
                await request.SendSimpleEmbed("Role Permission",
                    $"You did not give a valid role ID. Use ``!roles`` to list all current server roles, along with their ids");
                return;
            }

            var roleId = request.Parameters[0].AsUlong();
            if (!roleId.HasValue)
            {
                await request.SendSimpleEmbed("Role Permission",
                    $"You did not give a valid role ID. Use ``!roles`` to list all current server roles, along with their ids");
                return;
            }

            var role = serverChannel.Guild.GetRole(roleId.Value);
            if (role == null)
            {
                await request.SendSimpleEmbed("Role Permission", "No role with that id exists on this server");
                return;
            }

            try
            {
                await DatabaseRolePermissions.SetRolePermission(serverChannel.GuildId, roleId.Value, permissionLevel);
                PermissionValidator.UpdateCache(serverChannel.GuildId, roleId.Value, permissionLevel);
            }
            catch(Exception e)
            {
                await Logger.Main.LogError(e.Message);
            }
        }

        [Command("silencedrole", PermissionLevel.ServerOwner)]
        [CommandParameters(ParameterMatcher.ParameterType.Number)]
        [BlockUsageInPm, RequireParameterMatch]
        public async Task SetSilencedRole(CommandRequest request)
        {
            var val = request.Parameters[0].AsUlong();
            if (!val.HasValue)
            {
                await request.SendSimpleEmbed("Silenced Role", "Invalid role ID");
                return;
            }

            if (!(request.OriginalMessage.Channel is IGuildChannel channel))
                return;

            var role = channel.Guild.GetRole(val.Value);
            if (role == null)
            {
                await request.SendSimpleEmbed("Silenced Role", "No role by that ID exists on the server");
                return;
            }

            var setting = ServerSettingHandler.GetSettings(channel.GuildId);
            await setting.SetMutedRoleId(val.Value);
        }

    }
}