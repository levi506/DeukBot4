﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using DeukBot4.APIHandlers;
using DeukBot4.Database;
using DeukBot4.MessageHandlers.CommandHandler.RequestStructure;
using DeukBot4.MessageHandlers.Permissions;
using DeukBot4.Utilities;
using Discord;
using Discord.WebSocket;
#pragma warning disable 4014

namespace DeukBot4.MessageHandlers.CommandHandler
{
    public class GeneralCommands : CommandContainerBase
    {
        public override string Name => "General";

        [Command("info", PermissionLevel.Everyone)]
        [CommandHelp("Gives basic info on the bot", "Gives basic info on the bot")]
        public async Task Info(CommandRequest request)
        {
            await request.SendSimpleEmbed("Deukbot", "A bot designed by Deukhoofd for use on the Epsilon server", new[]
            {
                new EmbedFieldBuilder()
                {
                    Name = "Software", Value = "Deukbot 4.0", IsInline = true
                },
                new EmbedFieldBuilder()
                {
                    Name = "Creator", Value = "<@84372569012043776>", IsInline = true
                },
            });
        }

        [Command("ping", PermissionLevel.Everyone)]
        [CommandHelp("Simple Ping Pong Response", "Generates a simple Pong response when triggered")]
        public async Task Ping(CommandRequest request)
        {
            var t1 = DateTimeOffset.UtcNow;
            var eb = EmbedFactory.GetStandardEmbedBuilder();
            eb.Title       = "Pong";
            eb.Description = "Pong";
            eb.Fields = new List<EmbedFieldBuilder>()
            {
                new EmbedFieldBuilder()
                {
                    Name  = "Discord Reported Latency",
                    Value = $"{Program.Client.Latency} ms"
                },
                new EmbedFieldBuilder()
                {
                    Name = "Measured Ping between Message and Command Handling",
                    Value = $"{(int)(t1 - request.OriginalMessage.CreatedAt).TotalMilliseconds} ms"
                }
            };
            eb.Footer = new EmbedFooterBuilder()
            {
                Text = "Measured Ping can be off a bit due to Discord timestamping"
            };

            var t2 = DateTimeOffset.UtcNow;
            var m = await request.SendMessageAsync("", embed: eb.Build());
            eb.Fields.Add(new EmbedFieldBuilder()
            {
                Name  = "Measured Time handling embed creation",
                Value = $"{(t2 - t1).TotalMilliseconds} ms"
            });
            eb.Fields.Add(new EmbedFieldBuilder()
            {
                Name = "Measured Ping between Bot and Discord",
                Value = $"{(int)(m.CreatedAt - t2).TotalMilliseconds} ms"
            });


            m.ModifyAsync(properties => { properties.Embed = eb.Build(); });
        }

        [Command("help", PermissionLevel.Everyone)]
        [CommandParameters(new []{ParameterMatcher.ParameterType.Word})]
        [CommandHelp("Displays a list of commands for the bot",
            "Allows you to see all commands you can use for your permission level, along with a description.\n" +
            "usage:\n``help`` for a list of all commands useable for you.\n" +
            "``help`` [command name] for more detailed info on a specific command. " +
            "Note that you need to be able to use the command to get this info.")]
        public async Task Help(CommandRequest request)
        {
            if (request.Parameters.Length == 0)
            {
                await request.SendMessageAsync("",
                    embed: HelpCommandGenerator.GenerateFullHelp(request.RequestPermissions));
            }
            else
            {
                await request.SendMessageAsync("",
                    embed: HelpCommandGenerator.GenerateSpecificHelp(request.Parameters[0].AsString(),
                        request.RequestPermissions));
            }
        }

        [Command("whatdoyouthinkdeukbot", PermissionLevel.Everyone)]
        [Command("whatdoyouthink", PermissionLevel.Everyone)]
        [CommandHelp("Gives the bots opinion about something", "Gives the bots opinion about something\nusage:\n``whatdoyouthink {about something}``")]
        [CommandParameters(ParameterMatcher.ParameterType.Remainder)]
        public async Task BotOpinion(CommandRequest request)
        {
            await request.SendSimpleEmbed("Opinion", BotOpinions.GetOpinion(request));
        }

        [Command("catfact", PermissionLevel.Everyone)]
        [CommandHelp("Returns a random cat fact", "Returns a random cat fact")]
        public async Task RandomCatFact(CommandRequest request)
        {
            await request.SendSimpleEmbed("Cat Fact", await AnimalFactsApi.GetRandomCatFact("cat"));
        }

        [Command("dogfact", PermissionLevel.Everyone)]
        [CommandHelp("Returns a random dog fact", "Returns a random cat fact")]
        public async Task RandomDogFact(CommandRequest request)
        {
            await request.SendSimpleEmbed("Dog Fact", await AnimalFactsApi.GetRandomCatFact("dog"));
        }


        [Command("catpic", PermissionLevel.Everyone)]
        [CommandHelp("Returns a random cat picture", "Returns a random cat picture")]
        [CommandParameters(ParameterMatcher.ParameterType.Remainder)]
        public async Task RandomCatPc(CommandRequest request)
        {
            if (!string.IsNullOrWhiteSpace(request.Parameters[0].AsString()))
            {
                var saying = request.Parameters[0].AsString();
                await request.OriginalMessage.Channel.SendFileAsync(await CatPicHandler.GetCatPicture(saying), "cat_pic.png");
                return;
            }
            await request.OriginalMessage.Channel.SendFileAsync(await CatPicHandler.GetCatPicture(), "cat_pic.png");
        }

        [Command("avatar", PermissionLevel.Everyone)]
        [CommandHelp("Gets a users avatar", "Gets a users avatar. Returns avatar of user using the command if no user was specified.")]
        [CommandParameters(ParameterMatcher.ParameterType.User)]
        public async Task GetAvatar(CommandRequest request)
        {
            SocketUser user = null;
            if (request.Parameters.Length > 0)
            {
                var guild = (request.OriginalMessage.Channel as IGuildChannel)?.Guild;
                if (guild != null)
                {
                    user = (SocketUser) await request.Parameters[0].AsDiscordGuildUser(guild);
                }
                if (user == null)
                {
                    var userId = request.Parameters[0].AsUlong();
                    if (userId.HasValue)
                    {
                        user = Program.Client.GetUser(userId.Value);
                    }
                }
            }
            else
            {
                user = request.OriginalMessage.Author;
            }

            if (user == null)
            {
                await request.SendSimpleEmbed("Avatar", "Can't find that user");
                return;
            }
            var eb = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = $"{user.Username}#{user.Discriminator}",
                    IconUrl = user.GetAvatarUrl()
                },
                ImageUrl = user.GetAvatarUrl(ImageFormat.Auto, 256),
                Timestamp = request.OriginalMessage.CreatedAt,
                Color = Color.Gold
            };
            await request.SendMessageAsync("", embed: eb.Build());
        }

        [Command("user", PermissionLevel.Everyone)]
        [CommandHelp("Gets information on a user", "Returns detailed information on a user. Returns more information if the user is in the current server")]
        [CommandParameters(ParameterMatcher.ParameterType.User)]
        public async Task GetUser(CommandRequest request)
        {
            SocketUser user = null;
            if (request.Parameters.Length > 0)
            {
                var guild = (request.OriginalMessage.Channel as IGuildChannel)?.Guild;
                if (guild != null)
                {
                    user = (SocketUser) await request.Parameters[0].AsDiscordGuildUser(guild);
                }
                if (user == null)
                {
                    var userId = request.Parameters[0].AsUlong();
                    if (userId.HasValue)
                    {
                        user = Program.Client.GetUser(userId.Value);
                    }
                }
            }
            else
            {
                user = request.OriginalMessage.Author;
            }

            if (user == null)
            {
                await request.SendSimpleEmbed("Avatar", "Can't find that user");
                return;
            }

            var eb = EmbedFactory.GetStandardEmbedBuilder();
            eb.Title = $"{user.Username}#{user.Discriminator}";
            eb.Description = $"<@!{user.Id}>";
            eb.ThumbnailUrl = user.GetAvatarUrl();

            IGuildUser guildUser = null;
            if (request.OriginalMessage.Channel is IGuildChannel guildChannel)
            {
                guildUser = await guildChannel.Guild.GetUserAsync(user.Id);
            }

            if (guildUser?.Nickname != null)
                eb.AddField("Nickname", guildUser.Nickname, true);
            eb.AddField("Status", user.Status, true);
            eb.AddField("User Id", user.Id, true);
            eb.AddField("Is Bot", user.IsBot, true);
            eb.AddField("Joined Discord At", user.CreatedAt.ToString("D"), true);
            if (guildUser != null)
            {
                if (guildUser.JoinedAt.HasValue)
                    eb.AddField("Joined Server At", guildUser.JoinedAt.Value.ToString("D"), true);

                var bday = await BirthdayHandler.GetBirthday(guildUser.GuildId, guildUser.Id);
                if (bday.HasValue)
                {
                    eb.AddField("Birthday", bday.Value.ToString("D"), true);
                }

                var roles = guildUser.RoleIds;
                var sb = new StringBuilder();
                foreach (var roleId in roles)
                {
                    if (roleId == guildUser.GuildId)
                        continue;
                    sb.Append($"<@&{roleId}> ");
                }

                if (sb.Length > 0)
                    eb.AddField("Roles", sb.ToString());
            }


            await request.SendMessageAsync("", embed: eb.Build());
        }

        [Command("birthday", PermissionLevel.Everyone)]
        [CommandParameters(ParameterMatcher.ParameterType.Remainder)]
        [BlockUsageInPm]
        public async Task SetBirthday(CommandRequest request)
        {
            var guildChannel = request.OriginalMessage.Channel as IGuildChannel;
            if (guildChannel == null)
                return;
            if (request.Parameters.Length == 0)
            {
                await request.SendSimpleEmbed("Birthday", "You can use this command to set a birthday");
                return;
            }

            var parameter = request.Parameters[0].AsString();
            if (!DateTime.TryParseExact(parameter, "dd/MM/yyyy", CultureInfo.InvariantCulture,DateTimeStyles.None, out var bday))
            {
                await request.SendSimpleEmbed("Birthday",
                    "Couldn't parse that parameter as a date. Please rewrite the date string to format dd/MM/yyyy.");
                return;
            }

            BirthdayHandler.AddBirthday(bday, guildChannel.GuildId, request.OriginalMessage.Author.Id);
            await request.SendSimpleEmbed("Birthday", $"Success! Added {bday:dd MMMM yyyy} as your birthday!");
        }

        [Command("hug", PermissionLevel.Everyone)]
        [CommandParameters(ParameterMatcher.ParameterType.User)]
        [BlockUsageInPm, RequireParameterMatch]
        public async Task Hug(CommandRequest request)
        {
            var guildChannel = request.OriginalMessage.Channel as IGuildChannel;
            if (guildChannel == null)
                return;

            var user = await request.Parameters[0].AsDiscordGuildUser(guildChannel.Guild);
            if (user == null)
                return;
            string message;
            if (request.OriginalMessage.Author.Id == user.Id)
            {
                message = $"{user.Mention} hugs themselves. That's pretty sad to be honest.";
            }
            else if (user.Id == Program.BotId)
            {
                message = $"Please don't touch me.";
            }
            else
            {
                message = $"{request.OriginalMessage.Author.Mention} gives {user.Mention} a hug!";
                if (request.OriginalMessage.Author.Id == 355330726650052610)
                    message += " That's pt gay tbh.";
            }
            request.SendSimpleEmbed("Hug!", message);
        }

        [Command("points", PermissionLevel.Everyone)]
        [CommandParameters(ParameterMatcher.ParameterType.User)]
        [BlockUsageInPm]
        public async Task GetPoints(CommandRequest request)
        {
            if (!(request.OriginalMessage.Channel is IGuildChannel guildChannel))
                return;

            var user = (IGuildUser) request.OriginalMessage.Author;
            if (request.Parameters.Length > 0)
            {
                var u = await request.Parameters[0].AsDiscordGuildUser(guildChannel.Guild);
                if (u != null)
                    user = u;
            }

            var points = await PointHandler.GetPoints(guildChannel.GuildId, user.Id);
            if (points.HasValue)
            {
                request.SendSimpleEmbed("Points", $"You currently have {points.Value} points.");
            }
        }
    }
}