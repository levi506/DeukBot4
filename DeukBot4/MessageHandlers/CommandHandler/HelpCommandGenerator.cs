﻿using System.Collections.Generic;
using System.Text;
using DeukBot4.MessageHandlers.Permissions;
using Discord;

namespace DeukBot4.MessageHandlers.CommandHandler
{
    public static class HelpCommandGenerator
    {
        public static Embed GenerateFullHelp(PermissionLevel level)
        {
            var dic = new Dictionary<string, Dictionary<string, string>>();
            var eb = EmbedFactory.GetStandardEmbedBuilder();
            eb.Title = "Help";
            eb.Description = "Below you will find a list of commands you are able to use. " +
                             "For a more detailed information on a command use ``!help {command}``";
            foreach (var command in CommandHandler.Commands)
            {
                if (command.Value.Permission > level)
                    continue;
                if (!command.Value.HasHelp)
                    continue;
                if (command.Value.Alternatives.Contains(command.Key))
                    continue;
                if (!dic.TryGetValue(command.Value.CommandContainer.Name, out var entry))
                {
                    dic.Add(command.Value.CommandContainer.Name, new Dictionary<string, string>());
                    entry = dic[command.Value.CommandContainer.Name];
                }
                entry.Add(command.Value.Name, command.Value.ShortHelp);
            }
            foreach (var entry in dic)
            {
                var sectionSb = new StringBuilder();
                foreach (var cmd in entry.Value)
                {
                    sectionSb.Append($"**{cmd.Key}** -- {cmd.Value}\n");
                }
                eb.Fields.Add(new EmbedFieldBuilder()
                {
                    Name = entry.Key,
                    Value = sectionSb

                });
            }

            return eb.Build();
        }

        public static Embed GenerateSpecificHelp(string command, PermissionLevel level)
        {
            if (!CommandHandler.Commands.TryGetValue(command.ToLowerInvariant(), out var cmd))
                return null;
            if (cmd.Permission > level)
                return null;
            if (!cmd.HasHelp)
                return null;
            var eb = EmbedFactory.GetStandardEmbedBuilder();
            eb.Title = cmd.Name;
            eb.Description = cmd.LongHelp;

            return eb.Build();
        }
    }
}