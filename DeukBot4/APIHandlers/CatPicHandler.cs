﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace DeukBot4.APIHandlers
{
    public static class CatPicHandler
    {
        private const string URL = "https://cataas.com";

        public static async Task<Stream> GetCatPicture(string saying = null)
        {
            var fullUrl = "/cat";
            if (saying != null)
                fullUrl += "/says/" + HttpUtility.UrlPathEncode(saying);

            var client = new HttpClient {BaseAddress = new Uri(URL)};
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.GetAsync(fullUrl); // Blocking call!
            if (!response.IsSuccessStatusCode)
                throw new Exception("failure");

            var res = await response.Content.ReadAsStreamAsync();
            return res;
        }
    }
}