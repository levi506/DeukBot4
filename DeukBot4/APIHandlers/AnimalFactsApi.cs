﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace DeukBot4.APIHandlers
{
    public static class AnimalFactsApi
    {
        private const string URL = "https://api.levi506.net/";

        public static async Task<string> GetRandomCatFact(string animal)
        {
            var client = new HttpClient {BaseAddress = new Uri(URL)};
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.GetAsync("/v1/fact/animal/" + animal); // Blocking call!
            if (!response.IsSuccessStatusCode)
                return null;

            var res  = await response.Content.ReadAsStringAsync();
            var json = JObject.Parse(res);
            return json["fact"].Value<string>();

        }
    }
}