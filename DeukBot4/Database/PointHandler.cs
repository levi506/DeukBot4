using System;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace DeukBot4.Database
{
    public static class PointHandler
    {
        public static async Task<long?> GetPoints(ulong serverId, ulong userId)
        {
            var key = $"{serverId:D}-{userId:D}";
            try
            {
                var db     = ReminderHandler.Redis.GetDatabase();
                var exists = db.HashGet("points", (RedisValue) key);
                if (!exists.HasValue)
                {
                    return 0;
                }
                var points = long.Parse(exists);
                return points;
            }
            catch ( Exception e)
            {
                Logger.Main.LogError(e);
                return null;
            }
        }

        public static async Task<long?> ChangePoints(ulong serverId, ulong userId, long deltaPoints)
        {
            var key = $"{serverId:D}-{userId:D}";
            try
            {
                var db     = ReminderHandler.Redis.GetDatabase();
                var exists = db.HashGet("points", (RedisValue) key);
                if (!exists.HasValue)
                {
                    if (deltaPoints < -100)
                        deltaPoints = -100;
                    db.HashSet("points", (RedisValue) key, deltaPoints);
                    return deltaPoints;
                }

                var points = long.Parse(exists);

                points += deltaPoints;
                if (points < -100)
                    points = -100;
                db.HashSet("points", (RedisValue) key, points);
                return points;
            }
            catch ( Exception e)
            {
                Logger.Main.LogError(e);
                return null;
            }
        }
    }
}