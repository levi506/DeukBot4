﻿using System.Threading.Tasks;
using DeukBot4.MessageHandlers.Permissions;
using DeukBot4.Utilities;
using Npgsql;

namespace DeukBot4.Database
{
    public static class DatabaseRolePermissions
    {
        public static async Task SetRolePermission(ulong serverId, ulong roleId, PermissionLevel permissionLevel)
        {
            using (var conn = new DatabaseConnection())
            {
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "INSERT INTO permission_roles (server_id, role_id, permission_level)" +
                                      "VALUES (@server_id, @role_id, @permission_level) " +
                                      "ON CONFLICT (server_id, role_id) DO UPDATE SET permission_level = @permission_level";
                    cmd.Parameters.AddWithValue("server_id", serverId.ToLong());
                    cmd.Parameters.AddWithValue("role_id", roleId.ToLong());
                    cmd.Parameters.AddWithValue("permission_level", (sbyte)permissionLevel);
                    await cmd.ExecuteNonQueryAsync();
                }
            }
        }

        public static async Task<PermissionLevel> GetRolePermission(ulong serverId, ulong roleId)
        {
            using (var conn = new DatabaseConnection())
            {
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText =
                        "SELECT permission_level FROM permission_roles WHERE server_id = @server_id AND role_id = @role_id";
                    cmd.Parameters.AddWithValue("server_id", serverId.ToLong());
                    cmd.Parameters.AddWithValue("role_id", roleId.ToLong());

                    var reader = cmd.ExecuteReader();
                    while (await reader.ReadAsync())
                    {
                        return (PermissionLevel)(sbyte)reader.GetInt16(0);
                    }

                    return (PermissionLevel)sbyte.MinValue;
                }
            }
        }
    }
}