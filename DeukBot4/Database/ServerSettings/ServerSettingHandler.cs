﻿using System.Collections.Generic;
using System.Linq;
using DeukBot4.Utilities;
using Npgsql;

namespace DeukBot4.Database.ServerSettings
{
    public static class ServerSettingHandler
    {
        private static readonly Dictionary<ulong, ServerSetting> Settings = new Dictionary<ulong, ServerSetting>();

        public static void OnBotStartUp()
        {
            using (var conn = new DatabaseConnection())
            {
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection  = conn;
                    cmd.CommandText = "SELECT server_id, muted_role, enabled_jokes FROM server_settings";
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var id = reader.GetInt64(0).ToUlong();
                        var mutedRole = reader.GetInt64(1).ToUlong();
                        string[] enabledJokes = null;
                        if (!reader.IsDBNull(2))
                        {
                            enabledJokes = reader.GetString(2).Split(',');
                        }

                        Settings.Add(id, new ServerSetting(id, mutedRole, enabledJokes?.ToList()));
                    }
                }
            }
        }

        private static void AddSettingsToDatabase(ServerSetting setting)
        {
            using (var conn = new DatabaseConnection())
            {
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection  = conn;
                    cmd.CommandText =
                        "INSERT INTO server_settings (server_id, muted_role, enabled_jokes) VALUES (@id, @muted, @jokes)";
                    cmd.Parameters.AddWithValue("id", setting.ServerId.ToLong());
                    cmd.Parameters.AddWithValue("muted", setting.MutedRoleId.ToLong());
                    cmd.Parameters.AddWithValue("jokes", string.Join(",", setting.EnabledJokes));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static ServerSetting GetSettings(ulong id)
        {
            if (Settings.TryGetValue(id, out var setting))
            {
                return setting;
            }
            setting = new ServerSetting(id);
            Settings.Add(id, setting);
            AddSettingsToDatabase(setting);
            return setting;
        }
    }
}