﻿using System;
using System.Threading.Tasks;
using DeukBot4.Database;
using DeukBot4.Database.ServerSettings;
using DeukBot4.MessageHandlers;
using DeukBot4.MessageHandlers.CommandHandler;
using DeukBot4.MessageHandlers.JokeHandling;
using Discord;
using Discord.WebSocket;
using ReminderHandler = DeukBot4.Database.ReminderHandler;

namespace DeukBot4
{
    internal static class Program
    {
        public static DiscordSocketClient Client { get; private set; }
        public static Settings Settings { get; private set; }
        public static ulong BotId { get; private set; }
        public static bool IsConnected { get; private set; }

        private static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
        }

        private static async Task SetupScheduler()
        {
            ReminderHandler.Main.CheckReminders();
            BirthdayHandler.Initialize();
            BirthdayHandler.CheckBirthdays();
        }

        private static async Task MainAsync()
        {

            Settings = Settings.FromJsonFile("settings.json");
            DatabaseConnection.ConnectionString = Settings.DatabaseConnectionString;
            await SetupScheduler();
            JokeHandler.Initialize();

            DatabaseInitializer.Initialize();
            ServerSettingHandler.OnBotStartUp();
            TagStorage.BuildTags();
            CommandHandler.Build();
            ImageBackupHandler.SetBackupChannels(Settings.BackupChannels);
            ImageBackupHandler.Settings = Settings.WebDavSettings;

            Client = new DiscordSocketClient();

            Client.Log += Logger.Main.LogDiscord;
            Client.Ready += OnReady;
            Client.MessageReceived += MainHandler.HandleMessage;

            await Client.LoginAsync(TokenType.Bot, Settings.Token);
            await Client.StartAsync();


            // Block this task until the program is closed.
            await Task.Delay(-1);
        }

        private static async Task OnReady()
        {
            Client.CurrentUser.ModifyAsync(properties =>
            {
                properties.Username = Settings.Username;
                properties.Avatar = new Image("avatar.png");
            });
            EmbedFactory.Initialize(Client.CurrentUser);
            Console.WriteLine(("Joined Guilds: "));
            foreach (var clientGuild in Client.Guilds)
            {
                Console.WriteLine($"- {clientGuild.Id}: {clientGuild.Name}");
            }
            BotId = Client.CurrentUser.Id;
            IsConnected = true;
        }
    }
}