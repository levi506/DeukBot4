﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DeukBot4.Utilities
{
    public static class EnumerableExtensions
    {
        public static string Join(this IEnumerable<string> arr, string sep)
        {
            return string.Join(sep, arr);
        }

        public static T Choice<T>(this IEnumerable<T> arr, Random random)
        {
            var a = arr.ToArray();
            return a[random.Next(0, a.Count())];
        }
    }
}