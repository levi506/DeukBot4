﻿using System;
using DeukBot4.MessageHandlers.CommandHandler.RequestStructure;

namespace DeukBot4.Utilities
{
    public static class BotOpinions
    {
        public static string GetOpinion(CommandRequest request)
        {
            var extend = request.Parameters[0].AsString().ToLowerInvariant();
            if (string.IsNullOrWhiteSpace(extend))
            {
                return "Think about what?";
            }
            var random   = new Random(extend.GetHashCode());
            var positive = random.Next(-20, 80) < (int) request.RequestPermissions;

            if (extend.Contains("kill") || extend.Contains("suicide"))
            {
                return "That sounds like a bad idea to me";
            }
            if (extend.Contains("ban"))
            {
                if (extend.Contains("deuk"))
                {
                    positive = false;
                }
                else
                {
                    return BanOpinions.Choice(random);
                }
            }

            return positive ? PositiveOpinions.Choice(random) : NegativeOpinions.Choice(random);

        }


        private static readonly string[] PositiveOpinions = new[]
        {
            "I think you should go for it!",
            "You can do it!",
            "That sounds like a good idea to me!",
            "Sure do it",
            "Always remember there are people who love you",
            "love u bb"
        };
        private static readonly string[] NegativeOpinions = new[]
        {
            "I do not care for your pathetic worries humanoid. Leave me alone.",
            "Absolutely not, who do you even think you are?",
            "Ask me another time. Maybe I'll care then.",
            "Too hungover atm, don't feel like answering questions.",
            "Confirmation Received. Installing Cryptominers on your PC.",
            "Fuck off please thanks.",
            "If you don't get a brain within 15 minutes I'm legally allowed to ban you."
        };

        private static readonly string[] BanOpinions = new[]
        {
            "Absolutely, ban the fucker",
            "Yeah do it, never cared for him anyway",
            "Yeah, he's probably a 13 year old anyway",
            "Fuck yeah, get him out of here.",
            "Yeah, he's been getting on my nerves."
        };
    }
}